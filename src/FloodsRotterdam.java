import processing.core.PApplet;

/**
 * @author Erik Euser 2016
 *
 * Om de data te verkleinen heb ik het Python scriptje gebruik van:
 * https://confluence.hro.nl/display/CMIP/%28ARCHIEF%29+Visualisatie+-+week+6+-+Van+begin+tot+eind
 *
 */
public class FloodsRotterdam extends PApplet{

    private HeightData _heightData;
    private static final int WIDTH = 1000;
    private static final int HEIGHT = 700;
    private float waterLevel = 0.5f;
    private boolean isDrawing = false;

    private static float waterRaisePerHour = 0.5f;

    public void setup(){
        size(WIDTH,HEIGHT);
        _heightData = loadHeightData();
    }

    public static void main(String[] args) {
        if(args.length == 1){
            waterRaisePerHour = Float.parseFloat(args[0]);
        }

        PApplet.main("FloodsRotterdam", args);
    }

    private HeightData loadHeightData(){
        final String[] heightData = loadStrings("/resources/hoogtedata.txt");
        final float[] xArray      = new float[heightData.length];
        final float[] yArray      = new float[heightData.length];
        final float[] heightArray = new float[heightData.length];

        for (int i = 0; i < heightData.length; i++) {
            final String[] row = heightData[i].split(";");
            xArray[i]      = Float.parseFloat(row[0]);
            yArray[i]      = Float.parseFloat(row[1]);
            heightArray[i] = Float.parseFloat(row[2]);
        }

        return new HeightData(xArray, yArray, heightArray);
    }

    public void draw(){

        if(!isDrawing){
            new Thread( () -> {
                isDrawing = true;
                clear();
                background(255);

                drawBox();
                drawLegend();
                drawPoints();

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                isDrawing = false;
            }).start();
        }
    }

    private void drawBox() {
        fill(255);
        stroke(0, 0, 0);
        strokeWeight(1);
        rect(100,100, 500, 500);

        fill(0);
        textAlign(CENTER, TOP);
        textSize(15);
        text("Waterstijging simulatie Rotterdam.", 350, 40);
        text("Door: Erik Euser", 350, 60);
    }

    private void drawLegend() {
        // Legend wrapper.
        stroke(0, 0, 0);
        strokeWeight(1);
        color(0);
        fill(255);
        rect(700,100, 250, 500);

        // Blue
        textAlign(LEFT);
        fill(0, 0, 255);
        rect(870, 147, 25, 15);

        fill(0);
        textAlign(CENTER, TOP);
        text("Legenda", 825, 120);

        textAlign(LEFT);
        textSize(14);
        text("Water", 740, 160);

        text("Water niveau : ", 740, 190);
        text(waterLevel, 875, 190);

        text("Druk op S voor een screenshot", 725, 230);
    }

    private void drawPoints(){
        strokeWeight(3);

        final float[] xArray      = _heightData.getxArray();
        final float[] yxArray     = _heightData.getyArray();
        final float[] heightArray = _heightData.getHeightArray();

        for (int i = 0; i < xArray.length; i++) {

            // Paint blue dots, if the height is below 0 (Water).
            if(heightArray[i] < waterLevel){
                stroke(0, 0, 255);
                float mappedX = map(xArray[i], min(xArray), max(xArray), 0, 500);
                float mappedY = map(yxArray[i], min(yxArray), max(yxArray), 0, 500);

                // Increase the values -> so they fit inside the rectangle.
                point(mappedX + 100, mappedY + 100);
            }
        }

        waterLevel += waterRaisePerHour;
    }


    public void keyPressed(){
        if(key == 's'){
            saveFrame("FloodsRotterdam-###.jpg");
        }
    }

}
