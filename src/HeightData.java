/**
 * @author Erik Euser 2016
 *
 * Dataholder class
 *
 */
class HeightData {
    private float[] xArray;
    private float[] yArray;
    private float[] heightArray;

    HeightData(float[] xArray, float[] yArray, float[] heightArray) {
        this.xArray = xArray;
        this.yArray = yArray;
        this.heightArray = heightArray;
    }

    float[] getxArray() {
        return xArray;
    }

    float[] getyArray() {
        return yArray;
    }


    float[] getHeightArray() {
        return heightArray;
    }
}
