import csv
import sys

# Bron : https://confluence.hro.nl/pages/viewpage.action?spaceKey=CMIP&title=%28ARCHIEF%29+Visualisatie+-+week+6+-+Van+begin+tot+eind
zadkineX= 92800
zadkineY= 436955
delta= 4

  
linksX=zadkineX-delta
linksY=zadkineY-delta
rechtsX=zadkineX+delta
rechtsY=zadkineY+delta
  
w=rechtsX-linksX
h=rechtsY-linksY
  
 
data=[]
with open('rotterdamopendata_hoogtebestandtotaal_west.csv') as csvfile:    
    dialect = csv.Sniffer().sniff(csvfile.read(1024))    
    csvfile.seek(7)  
    reader = csv.reader(csvfile, dialect)    
  
for row in reader:                 
    x=int(float(row[0])) 
    y=int(float(row[1]))   
    z=float(row[2])      
    if linksX<x<rechtsX and linksY<y<rechtsX:
		data.append([x,y,z])

f = open('hoogtedata.txt', 'w') 
for x in data:   
    f.write(str(x[0])+";"+str(x[1])+";"+str(x[2])+'\n')